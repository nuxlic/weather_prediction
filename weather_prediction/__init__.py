# -*- coding: utf-8 -*-

"""Top-level package for Weather Prediction."""

__author__ = """Flavio Maximiliano Cangini"""
__email__ = 'cangini.f@hotmail.com'
__version__ = '0.1.0'
