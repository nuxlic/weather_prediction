from weather_prediction.meteorologic.galaxy.solar_system import SolarSystem


DROUGHT = 'drought'
RAINY = 'rainy'
OPTIMAL_PRESSURE = 'optimal pressure'
INDEFINITE = 'indefinite'


def predict_condition_for_day(day):
    prediction = {
        'day': day,
        'weather': None
    }
    solar_system = SolarSystem()
    solar_system.orbit_planets_n_days(day)
    rainy, perimeter = solar_system.rainy_period()
    if solar_system.drought_period():
        prediction['weather'] = DROUGHT
    elif solar_system.optimal_pressure_period():
        prediction['weather'] = OPTIMAL_PRESSURE
    elif rainy:
        prediction['weather'] = RAINY
        prediction['perimeter'] = perimeter
    else:
        prediction['weather'] = INDEFINITE

    return prediction


def predict_amount_of_periods(years):
    amount_of_periods = {
        DROUGHT: {'amount': 0},
        RAINY: {'amount': 0, 'max_rainy_perimeter': 0},
        OPTIMAL_PRESSURE: {'amount': 0},
        INDEFINITE: {'amount': 0}
    }
    for prediction in (predict_condition_for_day(day) for day in range(years * 360)):
        amount_of_periods[prediction['weather']]['amount'] += 1
        if prediction.get('perimeter', 0) > amount_of_periods[prediction['weather']].get('max_rainy_perimeter', 0):
            amount_of_periods[prediction['weather']]['max_rainy_perimeter'] = prediction['perimeter']

    return amount_of_periods
