import math
from collections import namedtuple
from operator import add, sub
from typing import Callable

Coordinates = namedtuple("Coordinates", ["x", "y"])


class Planet:

    def __init__(self, name: str, velocity: int, direction: Callable[[int, int], int],
                 distance: int, initial_position: int):
        self._name = name
        self._velocity = velocity
        self._direction = direction
        self._distance = distance
        self._current_position = initial_position
        self._default_coords = {
            0: Coordinates(self._distance, 0),
            90: Coordinates(0, self._distance),
            180: Coordinates(-self._distance, 0),
            270: Coordinates(0, -self._distance),
            360: Coordinates(self._distance, 0)
        }

    def spin_arround_the_sun_n_days(self, days: int):
        self._current_position = self._direction(self._current_position, self._velocity * days) % 360

    def get_current_coordinates(self):
        if self._current_position in self._default_coords:
            return self._default_coords[self._current_position]
        y = self._distance * math.sin(math.radians(self._current_position))
        x = self._distance * math.cos(math.radians(self._current_position))
        return Coordinates(x, y)

    def __str__(self):
        return f"Planet<{self.__dict__}>"


class SolarSystem:

    def __init__(self):
        self._planets = [
            Planet(name="Ferengi", velocity=1, direction=add, distance=500, initial_position=90),
            Planet(name="Betasoide", velocity=3, direction=add, distance=2000, initial_position=0),
            Planet(name="Vulcano", velocity=5, direction=sub, distance=1000, initial_position=0),
        ]

    def orbit_planets_n_days(self, days):
        for planet in self._planets:
            planet.spin_arround_the_sun_n_days(days)

    def drought_period(self):
        ferengi_coords, betasoide_coords, vulcano_coords = self._get_current_planets_coordinates()

        try:
            equation_of_straight_line = get_straight_line_function_between_two_planets(ferengi_coords,
                                                                                       betasoide_coords)
        except ZeroDivisionError:
            return vulcano_coords.x == 0

        sun_coords = Coordinates(0, 0)
        return equation_of_straight_line(sun_coords.x) == sun_coords.y and \
               equation_of_straight_line(vulcano_coords.x) == vulcano_coords.y

    def rainy_period(self):
        ferengi_coords, betasoide_coords, vulcano_coords = self._get_current_planets_coordinates()

        calculates_to_know_if_the_sun_is_in_the_triangle = [
            ferengi_coords.x * betasoide_coords.y - betasoide_coords.x * ferengi_coords.y,
            betasoide_coords.x * vulcano_coords.y - vulcano_coords.x * betasoide_coords.y,
            vulcano_coords.x * ferengi_coords.y - ferengi_coords.x * vulcano_coords.y
        ]
        if all(calculate > 0 for calculate in calculates_to_know_if_the_sun_is_in_the_triangle) or \
            all(calculate < 0 for calculate in calculates_to_know_if_the_sun_is_in_the_triangle):
            distance_between_ferengi_betazoide = calculate_distance_between_two_planets(ferengi_coords,
                                                                                        betasoide_coords)
            distance_between_betazoide_vulcano = calculate_distance_between_two_planets(betasoide_coords,
                                                                                        vulcano_coords)
            distance_between_ferengi_vulcano = calculate_distance_between_two_planets(ferengi_coords,
                                                                                      vulcano_coords)
            current_perimeter = distance_between_betazoide_vulcano + distance_between_ferengi_betazoide
            current_perimeter += distance_between_ferengi_vulcano
            return True, current_perimeter

        return False, None

    def optimal_pressure_period(self):
        ferengi_coords, betasoide_coords, vulcano_coords = self._get_current_planets_coordinates()

        try:
            equation_of_straight_line = get_straight_line_function_between_two_planets(ferengi_coords,
                                                                                       betasoide_coords)
        except ZeroDivisionError:
            return False

        sun_coords = Coordinates(0, 0)
        return equation_of_straight_line(sun_coords.x) != sun_coords.y and \
               equation_of_straight_line(vulcano_coords.x) == vulcano_coords.y

    def _get_current_planets_coordinates(self):
        return [planet.get_current_coordinates() for planet in self._planets]


def get_straight_line_function_between_two_planets(planet_coords, another_planet_coords):
    slope = (another_planet_coords.y - planet_coords.y) / (another_planet_coords.x - planet_coords.x)
    equation_of_straight_line = lambda x: slope * x - slope * planet_coords.x + planet_coords.y
    return equation_of_straight_line


def calculate_distance_between_two_planets(planet_coords, another_planet_coords):
    delta_x = math.pow(another_planet_coords.x - planet_coords.x, 2)
    delta_y = math.pow(another_planet_coords.y - planet_coords.y, 2)
    return math.sqrt(delta_x + delta_y)
